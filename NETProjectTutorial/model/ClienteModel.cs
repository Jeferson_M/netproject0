﻿using NETProjectTutorial.entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.model
{
    class ClienteModel
    {

        private static List<Cliente> clientes = new List<Cliente>();
        private implements.DaoImplementsCliente daocliente;

        public ClienteModel()
        {
            daocliente = new implements.DaoImplementsCliente();
        }


        public List<Cliente> GetClientes()
        {
            return daocliente.findAll();
        }

        public void Populate()
        {
            Cliente[] clts =
            {
                new Cliente(1,"002321654789U","Marco","Perez","22148963","marcoperez@hotmail.com","De la cañada"),
                new Cliente(2,"002321654789U","Luisa","Perez","22148963","luisaperez@hotmail.com","De la cañada"),
                new Cliente(3,"002321654789U","Julio","Perez","22148963","julioperez@hotmail.com","De la cañada")
            };

            //clientes = pdts.ToList();
            foreach (Cliente c in clientes)
            {
                daocliente.save(c);
            }
            clientes = clts.ToList();

        }
       

        }
    }
