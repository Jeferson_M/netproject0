﻿using NETProjectTutorial.entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.model
{
    class EmpleadoModel
    {
        private static List<Empleado> empleados = new List<Empleado>();


        public static List<Empleado> GetEmpleados()
        {
            return empleados;
        }

        public static void Populate()
        {
            //Empleado[] emps =
            //{
            //    new Empleado(1,"12345","123-123456-1234O","Armando","Paredes","A un lado de su vecino","2225-1116",
            //    "8888-8888",Empleado.SEXO.MASCULINO,25000),
            //    new Empleado(2,"67891","555-444444-7777U","Zoila","Vaca","Detras de done el vecino del Armando","2269-1177",
            //    "8887-7777",Empleado.SEXO.FEMENINO,25000),
            //    new Empleado(3,"12345","123-123456-1234O","Zacarias","Piedras del Rio","En la montala, rio arriba","2225-1116",
            //    "8888-8888",Empleado.SEXO.MASCULINO,25000)
            //};

            empleados = JsonConvert.DeserializeObject<List<Empleado>>
                (System.Text.Encoding.Default.GetString(NETProjectTutorial.Properties.Resources.MOCK_DATA));

            //empleados = emps.ToList();

        }
    }
}
