﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmCliente : Form
    {

        private DataTable tblClientes;
        private DataSet dsClientes;
        private BindingSource bsClientes;
        private DataRow drCliente;
        public FrmCliente()
        {
            InitializeComponent();
            bsClientes = new BindingSource();
        }
        public DataTable TblClientes { set { tblClientes = value; } }
        public DataSet DsClientes { set { dsClientes = value; } }

        public DataRow DrCliente
        {
            set
            {
                drCliente = value;
                
                txtCedula.Text = drCliente["Cedula"].ToString();
                txtNombre.Text = drCliente["Nombre"].ToString();
                TxtApellido.Text = drCliente["Apellido"].ToString();
                txtTelefono.Text = drCliente["Precio"].ToString();
                txtCorreo.Text = drCliente["Correo"].ToString();
                txtDir.Text = drCliente["Direccion"].ToString();
            }

        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            string cedula,nombre, apellido,telefono,correo, direccion;
           

           
            cedula = txtCedula.Text;
            nombre = txtNombre.Text;
            apellido = TxtApellido.Text;
            telefono = txtTelefono.Text;
            correo = txtCorreo.Text;
            direccion = txtDir.Text;

            if (drCliente != null)
            {
                DataRow drNew = tblClientes.NewRow();

                int index = tblClientes.Rows.IndexOf(drCliente);
                drNew["Id"] = drCliente["Id"];
                drNew["Cedula"] = cedula;
                drNew["Nombre"] = nombre;
                drNew["Apellido"] = apellido;
                drNew["Telefono"] = telefono;
                drNew["Correo"] = correo;
                drNew["Direccion"] = direccion;


                tblClientes.Rows.RemoveAt(index);
                tblClientes.Rows.InsertAt(drNew, index);

            }
            else
            {
                tblClientes.Rows.Add(tblClientes.Rows.Count + 1, cedula, nombre, apellido, telefono, correo,direccion);
            }

            Dispose();
        }

        private void FrmCliente_Load(object sender, EventArgs e)
        {
            bsClientes.DataSource = dsClientes;
            bsClientes.DataMember = dsClientes.Tables["Clientes"].TableName;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
    }
}
