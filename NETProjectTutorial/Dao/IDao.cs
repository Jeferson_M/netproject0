﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.Dao
{
    interface IDao<T>
    {
        void save(T t);
        int update(T t);
        bool Delete(T t);
        List<T> findAll();
    }
}
