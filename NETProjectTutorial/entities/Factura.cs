﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.entities
{
    class Factura
    {
        private int id;
        private string cod_factura;
        private DateTime fecha;
        private Empleado empleado;
        private Cliente cliente;
        private string observaciones;
        private double subtotal;
        private double iva;
        private double total;

        public Factura(int id, string cod_factura, DateTime fecha, Empleado empleado, Cliente cliente, string observaciones, double subtotal, double iva, double total)
        {
            this.id = id;
            this.cod_factura = cod_factura;
            this.fecha = fecha;
            this.empleado = empleado;
            this.observaciones = observaciones;
            this.subtotal = subtotal;
            this.iva = iva;
            this.total = total;
        }

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Cod_factura
        {
            get
            {
                return cod_factura;
            }

            set
            {
                cod_factura = value;
            }
        }

        public DateTime Fecha
        {
            get
            {
                return fecha;
            }

            set
            {
                fecha = value;
            }
        }

        internal Empleado Empleado
        {
            get
            {
                return empleado;
            }

            set
            {
                empleado = value;
            }
        }

        public string Observaciones
        {
            get
            {
                return observaciones;
            }

            set
            {
                observaciones = value;
            }
        }

        public double Subtotal
        {
            get
            {
                return subtotal;
            }

            set
            {
                subtotal = value;
            }
        }

        public double Iva
        {
            get
            {
                return iva;
            }

            set
            {
                iva = value;
            }
        }

        public double Total
        {
            get
            {
                return total;
            }

            set
            {
                total = value;
            }
        }

        internal Cliente Cliente
        {
            get
            {
                return cliente;
            }

            set
            {
                cliente = value;
            }
        }
    }
}
