﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmGestionCliente : Form
    {
        private DataSet dsClientes;
        private BindingSource bsClientes;

        public DataSet DsClientes { set { dsClientes = value; } }
        public FrmGestionCliente()
        {
            InitializeComponent();
            bsClientes = new BindingSource();
        }

        private void FrmGestionCliente_Load(object sender, EventArgs e)
        {
            bsClientes.DataSource = dsClientes;
            bsClientes.DataMember = dsClientes.Tables["Producto"].TableName;
            dgvclientes.DataSource = bsClientes;
            dgvclientes.AutoGenerateColumns = true;
        }

        private void btnnuevo_Click(object sender, EventArgs e)
        {
            FrmCliente frc = new FrmCliente();
            frc.TblClientes = dsClientes.Tables["Cliente"];
            frc.DsClientes = dsClientes;
            frc.ShowDialog();
        }

        private void btneditar_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = dgvclientes.SelectedRows;

            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "ERROR, debe seleccionar una fila de la tabla para poder editar", "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DataGridViewRow gridRow = rowCollection[0];
            DataRow drow = ((DataRowView)gridRow.DataBoundItem).Row;

            FrmCliente frc = new FrmCliente();
            frc.TblClientes = dsClientes.Tables["Cliente"];
            frc.DsClientes = dsClientes;
            //frc.DrClientes = drow;
            frc.ShowDialog();
            
        }

        private void btneliminar_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = dgvclientes.SelectedRows;

            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "ERROR, debe seleccionar una fila de la tabla para poder editar", "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DataGridViewRow gridRow = rowCollection[0];
            DataRow drow = ((DataRowView)gridRow.DataBoundItem).Row;

            DialogResult result = MessageBox.Show(this, "Realmente desea eliminar ese registro?", "Mensaje del Sistema", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                //DsProductos.Tables["Producto"].Rows.Remove(drow);
                MessageBox.Show(this, "Registro eliminado satisfactoriamente!", "Mensaje del Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
